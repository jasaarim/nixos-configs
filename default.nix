{ lib, ... }:

{
  cx11 = ./hetzner/cx11.nix;

  sshKeyFromGitLab = { username, sha256 ? null }:
    let
      url = "https://gitlab.com/${username}.keys";
      target = if (sha256 == null)
               then { inherit url; }
               else { inherit url sha256; };
      keyString = with builtins; (readFile (fetchurl target));
      words = lib.strings.splitString " " keyString;
    in
      (lib.elemAt words 0) + " " + (lib.elemAt words 1);

}
