# NixOS configs

This repository provides an abstraction layer to configure remote
NixOS machines.  It works with Hetzner Cloud CX11 virtual machines.

To install NixOS on the VM, mount a nixos-minimal ISO-image (Hetzner
cloud provides that), enter the browser console, and run

```
$ sudo -s
# curl -s https://gitlab.com/jasaarim/nixos-configs/-/raw/master/hetzner/init.sh?inline=false > init.sh
# bash init.sh
```

(On my computer pasting the above URL to the browser console changes
`?` to `/`, `:` to `;`, and `>` to `.`.) The file
`/mnt/etc/nixos/configuration.nix` is now the same as
`configuration.nix` at the root of this repo.

Edit `/mnt/etc/nixos/configuration.nix` to choose the desired
configuration from the commented parameters.  Modify also the
arguments for `configs.sshKeyFromGitLab` to add your own public SSH
key to the machine. Once the configuration is ready, run

```
nixos-install
reboot
```

Then unmount the installer, restart the machine, and SSH into it.
Below are descriptions of some of the configurations.


## Nginx server

You can setup either a static file server or a reverse proxy and use
Let's Encrypt certificates or self-signed certificates behind a
reverse proxy.  For example, to setup two static virtual hosts and a
reverse proxy with Let's Encrypt certificates, specify the options

```
networking.extraHosts = "10.0.0.5 domain.internal";

nginxVhosts = {
  "domain.example" = {
    "/" = "/var/www/example";
  };
  "sub.domain.example" = {
    "/" = "/var/www/example2";
    "/path/" = "https://domain.internal/";
  };
};
ACMEVhosts = [ "domain.example" "sub.domain.example" ];
ACMEEmail = "email@domain.example";
```

Setting up a Let's encrypt certificate may not succeed automatically.
If no valid certificate, run `systemctl restart acme-<domain>.service`
on the machine.

### Nginx with Nextcloud in the internal network

To set up a corresponding Nextcloud server on the VM at `10.0.0.5`,
specify

```
nextcloudHostname = "domain.internal";
nextcloudExtraDomains = [ "sub.domain.example" ];
nextcloudWebRoot = "/path";
selfSignedVhost = "domain.internal";
```


## Set up a Kubernetes cluster

This configuration extends https://nixos.wiki/wiki/Kubernetes and
adapts that to CX11 machines.  Note that all the machines should be in
a private network where they can communicate directly with each other.
I've been using IPs 10.1.1.2 and 10.1.1.3 following the example.

For example, to configure a Kubernetes master, use

```
kubeMasterIP = "10.1.1.2";
```

To configure a Kubernetes node, use

```
kubeMasterIP = "10.1.1.2";
kubeNodeIP = "10.1.1.3";
```

### After reboot

After the reboot on a master, run

```
# mkdir ~/.kube
# ln -s /etc/kubernetes/cluster-admin.kubeconfig ~/.kube/config
```

See that the master is up:

```
# kubectl cluster-info
# kubectl get nodes
```

To connect the node to the cluster, on the master run

```
# cat /var/lib/kubernetes/secrets/apitoken.secret
```

Use this token in the node:

```
# echo TOKEN | nixos-kubernetes-node-join
```

