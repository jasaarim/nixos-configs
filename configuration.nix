{ config, pkgs, lib, ... }:

let
  configs = (import (builtins.fetchGit {
    url = "https://gitlab.com/jasaarim/nixos-configs";
    ref = "master";
    rev = "baf02d1fae01a94f8bb6a580460099241f093bbb";
  }))
    { inherit lib; };
  sshKey = configs.sshKeyFromGitLab {
    username= "jasaarim";
    # You can omit the sha256 attribute and no hash will be checked.
    # To get the value, use
    # `nix-prefetch-url https://gitlab.com/<username>.keys --type sha256`
    sha256 = "122g1f3axzaqbk6z09z0nrnip72b6s6v4nf3hhdqsvn1jrd2rqlr";
  };
in
{
  imports = [
    ./hardware-configuration.nix
    configs.cx11
  ];

  # With a Kubernetes node or master, this will be ignored and the hostname
  # will the internal IP.
  #networking.hostName = HOSTNAME;
  # Domain for a host in the internal network (e.g., for proxying)
  #networking.extraHosts = "10.0.1.4 example.domain";

  # Nginx options
  #nginxVhosts = { VHOST1 = { PATH = TARGET;}; VHOST2 = { PATH = TARGET; };};
  #selfSignedVhost = VHOST;
  #ACMEVhosts = [ VHOST1 VHOST2 ];
  #ACMEEmail = EMAIL;

  # Nextcloud (Use selfSignedVhost or ACMEVhost with this)
  #nextcloudHostname = VHOST
  #nextcloudExtraDomains = [ VHOST2 ];

  # Kubernetes options
  #kubeMasterIP = IP;
  #kubeNodeIP = IP;

  users.users.root.openssh.authorizedKeys.keys = [ sshKey ];
}
