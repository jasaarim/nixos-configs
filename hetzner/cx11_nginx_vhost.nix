{ config, lib, ... }:

with lib;

let
  vhosts = config.nginxVhosts;
  mapVhost = vhost: paths: toVhostConfig paths;

  toVhostConfig = paths: {
    forceSSL = true;
    locations = builtins.mapAttrs (path: target:
      if (builtins.substring 0 1 target) == "/" then
        { root = target; }
      else
        proxyLocation target
    ) paths;
  };

  proxyLocation = target: {
    proxyPass = target;
    proxyWebsockets = true;
    extraConfig =
      # required when the target is also TLS server with multiple hosts
      "proxy_ssl_server_name on;" +
      # required when the server wants to use HTTP Authentication
      "proxy_pass_header Authorization;";
  };
in
{
  options = {
    nginxVhosts = mkOption {
      default = null;
      type = with types; nullOr (attrsOf (attrsOf str));
      description = "Configure static virtual hosts or proxies";
    };
  };

  config = mkIf (config.nginxVhosts != null) {
    services.nginx.virtualHosts = builtins.mapAttrs mapVhost vhosts;
  };
}
