#! /usr/bin/env bash
#
# Script to configure one Hetzner cloud CX11 virtual machine.
#
# This script should be run on a nixos-minimal installer that can be
# found from the list of available ISO images in the cloud console.
# After running, edit /mnt/etc/nixos/configuration.nix, run
# nixos-install and reboot.  Depending on the type of configuration,
# there may additional steps after the reboot.

set -e

export BASE_URL=https://gitlab.com/jasaarim/nixos-configs/-/raw/master/

parted /dev/sda -- mklabel msdos
parted /dev/sda -- mkpart primary 1MiB -2GiB
parted /dev/sda -- mkpart primary linux-swap -2GiB 100%

mkfs.ext4 -L nixos /dev/sda1
mkswap -L swap /dev/sda2

mount /dev/disk/by-label/nixos /mnt
swapon /dev/sda2

nixos-generate-config --root /mnt

curl -s ${BASE_URL}configuration.nix?inline=false > /mnt/etc/nixos/configuration.nix

# Install git in order to use fetchGit in configuration.nix
nix-env -iA nixos.git
