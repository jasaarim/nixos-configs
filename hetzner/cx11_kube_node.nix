{ config, pkgs, lib, ... }:

with lib;

let
  kubeMasterHostname = "api.kube";
  kubeMasterAPIServerPort = 443;
  clusterCidr = "10.2.0.0/16";
in
{
  options = {
    kubeNodeIp = mkOption {
      default = null;
      type = types.nullOr types.str;
      description = ''
        IP for a Kubernetes node (will also be the hostname of the VM).
      '';
    };
  };

  config = mkIf (config.kubeNodeIp != null) {

    assertions = [{
      assertion = config.kubeMasterIp != null;
      message = "kubeMasterIp is also needed with kubeNodeIp";
    }];

    environment.systemPackages = with pkgs; [
      kompose kubectl kubernetes
    ];

    networking = {
      hostName = mkForce config.kubeNodeIp;
      extraHosts = "${config.kubeMasterIp} ${kubeMasterHostname}";
    };

    # https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports
    # https://github.com/coreos/coreos-kubernetes/blob/master/Documentation/kubernetes-networking.md
    networking.firewall.interfaces.ens10 = {
      allowedTCPPorts = [ 10250 10255 ];
      allowedTCPPortRanges = [ { from = 30000; to = 32767; } ];
      allowedUDPPorts = [ 8285 8472 ];
    };

    # Despite modifying the network interface metrics, flannel defaults to ens3
    services.flannel.iface = "ens10";

    # Without this the certs lack SANs for IPs and the master doesn't
    # accept them
    services.certmgr.specs = {
      flannelClient.request.hosts = [ config.kubeNodeIp ];
      kubeProxyClient.request.hosts = [ config.kubeNodeIp ];
      kubelet.request.hosts = [ config.kubeNodeIp ];
      kubeletClient.request.hosts = [ config.kubeNodeIp ];
    };

    services.kubernetes = let
      api = "https://${kubeMasterHostname}:${toString kubeMasterAPIServerPort}";
    in
      {
        roles = ["node"];
        masterAddress = kubeMasterHostname;
        easyCerts = true;
        apiserverAddress = api;
        clusterCidr = clusterCidr;

        proxy.bindAddress = config.kubeNodeIp;

        # use coredns
        addons.dns.enable = true;

        kubelet = {
          kubeconfig.server = api;
          nodeIp = config.kubeNodeIp;
          extraOpts = "--fail-swap-on=false";
        };
      };
  };
}
