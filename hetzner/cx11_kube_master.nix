{ config, pkgs, lib, ... }:

with lib;

let
  kubeMasterHostname = "api.kube";
  kubeMasterAPIServerPort = 443;
  clusterCidr = "10.2.0.0/16";
in
{
  options = {
    kubeMasterIp = mkOption {
      default = null;
      type = types.nullOr types.str;
      description = ''
        IP for a Kubernetes master (will also be the hostname of the master VM).
        If only this is specified and not kubeNodeIp, a master will be built.
      '';
    };
  };

  config = mkIf (config.kubeMasterIp != null && config.kubeNodeIp == null) {
    environment.systemPackages = with pkgs; [
      kompose kubectl kubernetes
    ];

    networking = {
      hostName = mkForce config.kubeMasterIp;
      extraHosts = "${config.kubeMasterIp} ${kubeMasterHostname}";
    };

    # https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports
    # https://github.com/coreos/coreos-kubernetes/blob/master/Documentation/kubernetes-networking.md
    # Port 8888 is for cfssl
    networking.firewall.interfaces.ens10.allowedTCPPorts =
      [ 443 2379 2380 8888 10250 10251 10252 ];
    networking.firewall.interfaces.ens10.allowedUDPPorts = [ 8285 8472 ];

    services.flannel.iface = "ens10";

    services.kubernetes = {
      roles = ["master"];
      masterAddress = kubeMasterHostname;
      easyCerts = true;
      apiserver = {
        securePort = kubeMasterAPIServerPort;
        advertiseAddress = config.kubeMasterIp;
      };

      proxy.bindAddress = config.kubeMasterIp;

      clusterCidr = clusterCidr;

      addons.dns.enable = true;

      kubelet = {
        nodeIp = config.kubeMasterIp;
        extraOpts = "--fail-swap-on=false";
      };
    };
  };
}
