{ config, pkgs, ... }:

{
  imports = [
    ./cx11_nginx.nix
    ./cx11_nginx_vhost.nix
    ./cx11_nginx_acme.nix
    ./cx11_nginx_self_signed.nix
    ./cx11_nextcloud.nix
    ./cx11_kube_master.nix
    ./cx11_kube_node.nix
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  networking = {
    interfaces = {
      ens3.useDHCP = true;
      ens10.useDHCP = true;
    };
    # Prefer the internal network (ens10)
    dhcpcd.extraConfig = "
      interface ens3
      metric 203

      interface ens10
      metric 202
   ";
  };

  # Make a font slightly bigger in case we have to use the browser terminal
  console = {
    font = "Lat2-Terminus18";
    keyMap = "us";
  };

  time.timeZone = "Europe/Helsinki";

  services.openssh = {
    enable = true;
    permitRootLogin = "prohibit-password";
    passwordAuthentication = false;
  };

  environment.systemPackages = with pkgs; [
    git vim
  ];

  system.stateVersion = "22.11";
}
