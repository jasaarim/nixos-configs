{ config, pkgs, lib, ... }:

with lib;

{
  options = {
    selfSignedVhost = mkOption {
      default = null;
      type = types.nullOr types.str;
      description = "Virtual host with self-signed SSL";
    };
  };

  config = mkIf (config.selfSignedVhost != null) {
    # Allow only HTTPS and only from the internal network
    # Still allow SSH.
    networking.firewall = {
      allowedTCPPorts = mkForce [ 22 ];
      interfaces.ens10.allowedTCPPorts = [ 443 ];
    };

    environment.systemPackages = [ pkgs.openssl ];

    services.nginx = {
      virtualHosts.${config.selfSignedVhost} = {
        sslCertificate = "/etc/ssl/certs/cert.pem";
        sslCertificateKey = "/etc/ssl/private/key.pem";
        onlySSL = true;
        forceSSL = mkForce false;
      };
    };

    systemd.services.ssl-cert = {
      description = "Self-signed certificate for Nginx";
      requiredBy = [ "nginx.service" ];
      serviceConfig = { Type = "oneshot"; };
      script = ''
        mkdir -p   /etc/ssl/private
        ${pkgs.openssl}/bin/openssl req -x509 -nodes \
          -newkey rsa:2048 -keyout /etc/ssl/private/key.pem \
          -out /etc/ssl/certs/cert.pem -days 365 \
          -subj '/CN=${config.selfSignedVhost}'
        chmod 400 /etc/ssl/private/key.pem
        chown nginx:nginx /etc/ssl/private/key.pem
      '';
    };
  };
}
