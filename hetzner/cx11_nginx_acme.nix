{ config, lib, ... }:

with lib;

let
  vhosts = config.ACMEVhosts;
  # Convert the list of virtual hosts to attributes
  mapVhostToValue = vhosts: value: builtins.listToAttrs
    (map (name: { inherit name value; }) vhosts);
in
{
  options = {
    ACMEVhosts = mkOption {
      default = null;
      type = with types; nullOr (listOf str);
      description = "Virtual hosts for Let's Encrypt certificates";
    };

    ACMEEmail = mkOption {
      default = null;
      type = types.nullOr types.str;
      description = "Email for Let's Encrypt certificates";
    };
  };

  config = mkIf (vhosts != null) {
    assertions = [{
      assertion = config.ACMEEmail != null;
      message = "ACMEEmail needed with ACMEVhosts";
    }];

    security.acme.acceptTerms = true;

    security.acme.certs =  mapVhostToValue vhosts { email = config.ACMEEmail; };
    services.nginx.virtualHosts = mapVhostToValue vhosts { enableACME = true; };
  };
}
