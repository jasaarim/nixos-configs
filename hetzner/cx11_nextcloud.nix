{ config, pkgs, lib, ... }:

with lib;

{
  options = {
    nextcloudHostname = mkOption {
      default = null;
      type = types.nullOr types.str;
      description = "Hostname for the nextcloud server app";
    };

    nextcloudExtraDomains = mkOption {
      default = [];
      type = types.listOf types.str;
      description = "Extra domains that Nextcloud can be reached from";
    };

    nextcloudWebRoot = mkOption {
      default = null;
      type = types.nullOr types.str;
      description = "Optionally change the root URI or nextcloud from /";
    };
  };

  config = mkIf (config.nextcloudHostname != null) {
    services.nextcloud = {
      enable = true;
      nginx.enable = true;
      hostName = config.nextcloudHostname;
      https = true;
      config = {
        dbtype = "pgsql";
        dbuser = "nextcloud";
        dbhost = "/run/postgresql";
        dbname = "nextcloud";
        # These files with passwords need to exist for user nextcloud
        dbpassFile = "/var/nextcloud-db-pass";
        adminpassFile = "/var/nextcloud-admin-pass";
        adminuser = "admin";

        overwriteProtocol = "https";
        extraTrustedDomains = config.nextcloudExtraDomains;
      };
    };

    services.postgresql = {
      enable = true;
      ensureDatabases = [ "nextcloud" ];
      ensureUsers = [{
        name = "nextcloud";
        ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
      }];
    };

    systemd.services.nextcloud-setup = {
      requires = ["postgresql.service"];
      after = ["postgresql.service"];
    };

    systemd.services.nextcloud-webroot = mkIf (config.nextcloudWebRoot != null) {
      description = "Add custom webroot to Nextcloud configuration";
      requires = [ "nextcloud-setup.service" ];
      after = [ "nextcloud-setup.service" ];
      before = [ "phpfpm-nextcloud.service" ];
      serviceConfig = { Type = "oneshot"; };
      script = ''
        if grep -q "overwritewebroot" /var/lib/nextcloud/config/config.php
        then
          echo "overwritewebroot exists in config.php already"
        else
          sed -i "\$i \  'overwritewebroot' => '${config.nextcloudWebRoot}'," \
              /var/lib/nextcloud/config/config.php
        fi
      '';
    };
  };
}
